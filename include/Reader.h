#ifndef READER_H
#define READER_H

#include "utils.h"
#include <string>
#include <math.h>

UINT * getByteFrequencies(UINT wordSize, const std::string& filename);
#endif // READER_H
