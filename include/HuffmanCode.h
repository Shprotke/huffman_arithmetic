#ifndef HUFFMANCODE_H
#define HUFFMANCODE_H
#include "utils.h"
#include <queue>
#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <math.h>



class HuffmanCode
{
    public:
        HuffmanCode();
        HuffmanCode(UINT value, UINT frequency);

        // compare class required for priority_queue construction
        std::map<UINT, std::string>  HuffmanCodes(const std::vector<UINT> &data, const std::vector<UINT>& freq);

    private:

        UINT data;
        UINT freq;
        HuffmanCode *left;
        HuffmanCode *right;
        std::map<UINT, std::string> codes;
         struct Compare {
            bool operator()(HuffmanCode *node1, HuffmanCode* node2){
                return node1->freq > node2->freq;
            }
        };
        HuffmanCode* generateTree(std::priority_queue<HuffmanCode*, std::vector<HuffmanCode*> , Compare> p_queue);
        void getNodeData(HuffmanCode* root, UINT code[], UINT top);

};

bool encodeHuffman(const std::string &sourcefile, const std::string &destfile, std::map<UINT, std::string> codeMap, UINT wordSize);
UINT writeEncodedFileHeader(UINT wordSize, UINT filesize, std::map<UINT, std::string> codeMap);
bool decodeHuffman(const std::string &sourcefile, const std::string &destfile);

#endif // HUFFMANCODE_H
