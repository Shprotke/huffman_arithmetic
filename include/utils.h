#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#define UINT unsigned int
#define UCHAR unsigned char
#define BYTE_COUNT 256
#define BYTE_SIZE 8
#define MAX_TREE_HEIGHT 32
#define BUFFER_SIZE 100000000

#endif // UTILS_H_INCLUDED
