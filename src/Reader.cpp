#include "../include/Reader.h"
#include<iostream>
#include<fstream>

UINT * getByteFrequencies(UINT wordSize, const std::string& filename)
{
    UINT wordCount = (UINT)(pow(2, wordSize));
    UINT * frequencies = (UINT*) calloc(wordCount, sizeof(UINT));
    std::ifstream readStream(filename, std::ios::binary);

    if (frequencies == nullptr || !readStream.is_open())
    {
        std::cout<<"ERROR"<<std::endl;
        return 0;
    }

    // set stream position to beginning
    readStream.seekg(0, std::ios_base::seekdir::_S_beg);

    UINT wordIdx = wordSize - 1;
    UINT word = 0;
    UCHAR readChar = 0;
    while(readStream >> std::noskipws >> readChar)
    {
        for (int idx = BYTE_SIZE - 1; idx >= 0; idx--)
        {
            UINT lastBit = ((readChar >> idx) & 1); // 1 or 0
           // std::cout<<lastBit;

            word += lastBit * (pow(2, wordIdx)); //010 110 00

            if (wordIdx == 0)
            {
                wordIdx = wordSize - 1;
                frequencies[word]++;
                //std::cout<<" word value: "<<word<<std::endl;
                word = 0;
            }
            else
            {
                wordIdx--;
            }
        }
        //std::cout<<" ";
    }

    // filesize % k != 0
    // do we need to save this info?
    if (wordIdx != wordSize - 1)
    {
        std::cout<<"last word: "<<word<<std::endl;
        frequencies[word]++;
    }
    //std::cout<<"\n";
    return frequencies;
}
