#include "HuffmanCode.h"
#include <iostream>
#include <math.h>

HuffmanCode::HuffmanCode()
{
}

HuffmanCode::HuffmanCode(UINT value, UINT frequency)
{
    data = value;
    freq = frequency;
    left = right = nullptr; // or NULL
}
HuffmanCode* HuffmanCode::generateTree(std::priority_queue<HuffmanCode*,
                                   std::vector<HuffmanCode*>, Compare> p_queue)
{
    // keep calculating until only one node is left
    while (p_queue.size() != 1)
    {
        // remove first two elements
        // with highest priority - lowest frequency
        HuffmanCode* left = p_queue.top();
        p_queue.pop();

        HuffmanCode* right = p_queue.top();
        p_queue.pop();

        // form a new node from these nodes
        HuffmanCode* node = new HuffmanCode(0, left->freq + right->freq);
        node->left = left;
        node->right = right;
        p_queue.push(node);
    }
    return p_queue.top();
}
void HuffmanCode::getNodeData(HuffmanCode* root, UINT code[], UINT top)
{

    if (root->left) {
        code[top] = 0;
        getNodeData(root->left,
                   code, top + 1);
    }

    if (root->right) {
        code[top] = 1;
        getNodeData(root->right, code, top + 1);
    }

    if (!root->left && !root->right) {
        std::string value = "";
        for (UINT index = 0; index < top; index++) {
            value += (char)(code[index] + 48);
        }
        codes[root->data] = value;
    }
}
std::map<UINT,std::string> HuffmanCode::HuffmanCodes(const std::vector<UINT>& data,const std::vector<UINT>& freq)
{
    std::priority_queue<HuffmanCode*, std::vector<HuffmanCode*>, Compare> p_queue;
    for (UINT index = 0; index < data.size(); index++){
        HuffmanCode* node = new HuffmanCode(data[index], freq[index]);
        p_queue.push(node);
    }
    // generate encoding
    HuffmanCode* root = generateTree(p_queue);
    UINT arr[MAX_TREE_HEIGHT], top = 0;

    // fill codes
    getNodeData(root, arr, top);
    return codes;
}

/// write k, og filesize, amount of dictionary entries? and codeWord - originalByte dictionary (UINT: UINT)
/// "1001010011", 548653
/// "1010": 5483
/// 3 kodo baitai: 00000101, 00000000, 00001010
/// 00000101
/// efektyviausia vietos atzvilgiu turbut kodo zodzius koduot UINT'u taip: pirmi 8 bitai nurodo kodo zodzio ilgi x
/// paskutiniai x bitu skirti paciam kodo zodziui.
/// nuskaityta UINT'ą i kodo zodi konvertuojam taip:
/**
UINT readUint; // IS FAILO NUSKAITYTAS UINT'AS
UINT codeLength = readUint >> 3*BYTE_SIZE;
std::string codeString(codeLength, '0');
        for (int idx = codeLength - 1; idx >= 0; idx--)
        {
            UINT lastBit = ((readUint >> idx) & 1); // 1 or 0
            if (lastBit)
            {
                codeString[(codeLength - 1) - idx] = '1';
            }
        } f: adasddddddddddddddddd


**/

UINT writeEncodedFileHeader(UINT wordSize, const std::string &sourcefile, std::ofstream& writeStream, std::map<UINT, std::string> codeMap)
{
    std::ifstream readStream(sourcefile, std::ifstream::ate | std::ifstream::binary);
    if (!readStream.is_open()) {
        std::cout<<"ERROR: can't open source file."<<std::endl;
        return 0;
    }
    UINT filesize = readStream.tellg();
    UINT dictSize = codeMap.size();
    readStream.close();

    writeStream.write(reinterpret_cast<const char *>(&wordSize), sizeof(wordSize));
    writeStream.write(reinterpret_cast<const char *>(&filesize), sizeof(filesize));
    writeStream.write(reinterpret_cast<const char *>(&dictSize), sizeof(dictSize));

    for(auto entry : codeMap)
    {
        // convert string code into 3 bytes

        UINT length = entry.second.size();
        UCHAR codeLength = length & 0xFF;
        unsigned short code = 0;

        for (UINT idx = 0; idx < codeLength; idx++)
        {
            if (entry.second[idx] == '1')
            {
                code += pow(2, codeLength - 1 - idx);
            }

        }


        writeStream.write(reinterpret_cast<const char *>(&codeLength), sizeof(codeLength));
        writeStream.write(reinterpret_cast<const char *>(&code), sizeof(code));
        writeStream.write(reinterpret_cast<const char *>(&(entry.first)), sizeof(entry.first));



        // write 15 code bits as unsigned short
        /// TODO: change all uints into ushorts for encoding.

    }

    return filesize;
}

bool encodeHuffman(const std::string &sourcefile, const std::string &destfile, std::map<UINT, std::string> codeMap, UINT wordSize)
{
    std::ofstream writeStream(destfile, std::ofstream::binary);
    if (!writeStream.is_open()) {
        std::cout<<"ERROR: can't open dest file."<<std::endl;
        return false;
    }

    UINT filesize = writeEncodedFileHeader(wordSize, sourcefile, writeStream, codeMap);
    if (!filesize)
    {
        std::cout<<"ERROR: writing file header failed."<<std::endl;
        writeStream.close();
        return false;
    }

    std::ifstream readStream(sourcefile, std::ifstream::binary);


    if (!readStream.is_open()) {
        std::cout<<"ERROR: can't open source file."<<std::endl;
        writeStream.close();
        return false;
    }

    readStream.seekg(0, std::ios_base::seekdir::_S_beg);
    std::string bitString = "";
    UCHAR readChar;
    UINT word = 0;
    UINT wordIdx = wordSize - 1;
    std::vector<UCHAR> buffer;
    UCHAR currentByte = 0;
    UINT bytesRead = 0;
    while(readStream >> std::noskipws >> readChar)
    {
        bytesRead++;
        for (int idx = BYTE_SIZE - 1; idx >= 0; idx--)
        {
            UINT lastBit = ((readChar >> idx) & 1); // 1 or 0
            //std::cout<<lastBit;

            word += lastBit * (pow(2, wordIdx)); //010 110 00

            if (wordIdx == 0)
            {
                if (codeMap.find(word) == codeMap.end()) {
                    std::cout<<"ERROR: can't find word in encode map."<<std::endl;
                    return false;
                } else {
                    bitString += codeMap[word];
                    while (bitString.size() >= BYTE_SIZE)
                    {
                        currentByte = 0;
                        for (int bitIndex = 0; bitIndex < BYTE_SIZE; bitIndex++)
                        {
                            currentByte += (bitString[bitIndex] - '0') * (pow(2, (BYTE_SIZE - 1) - bitIndex));
                        }
                        buffer.push_back(currentByte);
                        bitString = bitString.substr(BYTE_SIZE);
                        if (buffer.size() >= BUFFER_SIZE)
                        {
                            writeStream.write(reinterpret_cast<const char*>(&buffer[0]), buffer.size()*sizeof(UCHAR));
                            buffer.clear();
                        }
                    }


                }
                wordIdx = wordSize - 1;
                word = 0;
            }
            else
            {
                wordIdx--;
            }
        }
    }

    if (wordIdx != wordSize - 1)
    { /// when file size in bits is not a multiple of k
    /// write the code of current word into buffer
        if (codeMap.find(word) == codeMap.end())
        {
            std::cout<<"ERROR: can't find word in encode map."<<std::endl;
            return false;
        } else
        {
            bitString += codeMap[word];
            std::cout<<"bit string length: "<<bitString.size();
            while (bitString.size() >= BYTE_SIZE)
            {
                currentByte = 0;
                for (int bitIndex = 0; bitIndex < BYTE_SIZE; bitIndex++)
                {
                    currentByte += (bitString[bitIndex] - '0') * (pow(2, (BYTE_SIZE - 1) - bitIndex));
                }
                buffer.push_back(currentByte);
                bitString = bitString.substr(BYTE_SIZE);
                if (buffer.size() >= BUFFER_SIZE)
                {
                    writeStream.write(reinterpret_cast<const char*>(&buffer[0]), buffer.size()*sizeof(UCHAR));
                    buffer.clear();
                }
            }


        }

    }

    if (bitString.size() != 0) {
        std::cout<<"bit string: "<<bitString<<std::endl;
        UCHAR currentByte = 0;
        for (UINT bitIndex = 0; bitIndex < bitString.size(); bitIndex++)
        {
            currentByte += (bitString[bitIndex] - '0') * (pow(2, (BYTE_SIZE - 1) - bitIndex));
        }
        buffer.push_back(currentByte);
        std::cout<<(UINT)buffer[buffer.size() - 2]<<", "<<(UINT)buffer[buffer.size() - 1]<<std::endl;
    }

    if (buffer.size() > 0){
        writeStream.write(reinterpret_cast<const char*>(&buffer[0]), buffer.size()*sizeof(UCHAR));
    }
    // close files
    readStream.close();
    writeStream.close();
    return true;
}

/// cia nuskaitysim k, irasytu kodo zodziu skaiciu, zodyno dydi ir is eiles visa zodyna
/// beigi pakeliui konvertuosim UINT'us i string'us patogumo delei.
/// Tada pereisim prie dekodavimo. Jis vyks labai panasiai kaip kodavimas.
/// Paprasciausias variantas: skaitom baita - > rasom pabiciui i stringa currentCodeString, po kiekvieno bito tikrinam ar neradom
/// string kodo zodyne, kai randam, pridedam prie decodedString zodynas[currentCodeString]:: is UINT paimam paskutinius k bitu ir juos pridedam.
/// Pakolei decodedString ilgesne uz baita,
/// konvertuojam ja i baitus ir rasom i dekoduota faila. Tada istustinam currentCodeString ir vaziuojam toliau.
bool decodeHuffman(const std::string &sourcefile, const std::string &destfile)
{ //read header and parse dictionary
    std::ifstream readStream(sourcefile, std::ios::binary);

    if (!readStream.is_open()) {
        std::cout<<"ERROR: can't open source file."<<std::endl;
        return false;
    }
    readStream.seekg(0, std::ios_base::seekdir::_S_beg);
    UINT wordSize = 0;
    UINT filesize = 0;
    UINT codeMapSize = 0;

    readStream.read((char*)(&wordSize), sizeof(wordSize));
    readStream.read((char*)(&filesize), sizeof(filesize));
    readStream.read((char*)(&codeMapSize), sizeof(codeMapSize));

    UCHAR codeLength = 0;
    unsigned short code = 0;
    UINT ogWord = 0;
    std::map<std::string, UINT> codeMap;

    for (UINT idx = 0; idx < codeMapSize; idx++)
    {
        readStream.read((char *)(&codeLength), sizeof(codeLength));
        readStream.read((char *)(&code), sizeof(code));

        std::string codeString(codeLength, '0');

        for (int bitIdx = codeLength - 1; bitIdx >= 0; bitIdx--)
        {
            UINT lastBit = ((code >> bitIdx) & 1); // 1 or 0
            if (lastBit)
            {
                codeString[(codeLength - 1) - bitIdx] = '1';
            }
        }
        /// !! we only need k last bits from ogWord/codeMap[codeString]
        readStream.read((char *)(&ogWord), sizeof(ogWord));

        codeMap[codeString] = ogWord;

    }

    UINT readBytes = 0;
    UCHAR readChar = 0;

    std::string decodedString = "";
    std::string currentCodeString = "";
    std::string bitBufferString = "";
    std::vector<UCHAR> buffer;

    std::ofstream writeStream(destfile, std::ofstream::binary);
    if (!writeStream.is_open()) {
        std::cout<<"ERROR: can't open dest file."<<std::endl;
        return false;
    }

    ///UINT codeLengthInt = (int) wordSize;
    while(readBytes < filesize && readStream >> std::noskipws >>readChar)
    {
        for (int idx = BYTE_SIZE - 1; idx >= 0; idx--)
        {
            UINT lastBit = ((readChar >> idx) & 1); // 1 or 0

            currentCodeString += lastBit ? '1' : '0';
            if (codeMap.find(currentCodeString) != codeMap.end())
            {
                /// we should only write k least significant bits from the dict entry

                /// append least significant k bits to bitBufferString
                UINT dictEntry = codeMap[currentCodeString];
                for (int bitIdx = wordSize - 1; bitIdx >= 0; bitIdx--)
                {
                    UINT lastBit = ((dictEntry >> bitIdx) & 1); // 1 or 0
                    if (lastBit)
                    {
                        bitBufferString += '1';
                    }
                    else
                    {
                        bitBufferString += '0';
                    }
                }

                /// while the buffer string is longer than a byte, append it's first bytes to a byteBuffer
                /// but only until we reach og filesize
                /// this way we know we can always write the full buffer and not worry about it holding data we did not encode
                while (bitBufferString.size() >= BYTE_SIZE && readBytes < filesize)
                {
                    /// is incrementing read byte count here good enough?
                    readBytes++;
                    UCHAR currentByte = 0;
                    std::string currentByteStr = bitBufferString.substr(0, BYTE_SIZE);
                    for (int bitIndex = 0; bitIndex < BYTE_SIZE; bitIndex++)
                    {
                        currentByte += (bitBufferString[bitIndex] - '0') * (pow(2, (BYTE_SIZE - 1) - bitIndex));
                    }
                    buffer.push_back(currentByte);
                    bitBufferString = bitBufferString.substr(BYTE_SIZE);

                    /// then write buffer when it's size reaches a limit
                    if (buffer.size() >= BUFFER_SIZE)
                    {
                        writeStream.write(reinterpret_cast<const char*>(&buffer[0]), buffer.size()*sizeof(UCHAR));
                        buffer.clear();
                    }
                }

                currentCodeString = "";
            }

        }
    }

    std::cout<<"currentCodeString: "<<currentCodeString<<std::endl;



    /// if the buffer is not empty, write
    if (buffer.size() > 0){
        writeStream.write(reinterpret_cast<const char*>(&buffer[0]), buffer.size()*sizeof(UCHAR));
    }

    readStream.close();
    writeStream.close();

    return true;


}
