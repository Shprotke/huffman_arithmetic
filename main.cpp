#include <iostream>
#include "include/Reader.h"
#include <bits/stdc++.h>
#include <vector>
#include "include/HuffmanCode.h"

int main()
{

    UINT wordSize = 5;
    std::string filename = "photo.jpg";
    UINT* frequencies = getByteFrequencies(wordSize, filename);
    // we need a map
    std::vector<UINT> v_frequencies;
    std::vector<UINT> v_data;
    for (UINT idx = 0; idx < (pow(2,wordSize)); idx++)
    {
        if (!frequencies[idx])
            continue;

        v_frequencies.push_back(frequencies[idx]);
        v_data.push_back(idx);
    }
    std::map<UINT, std::string> codes; // value -> kodavimas
    HuffmanCode h_code;
    codes = h_code.HuffmanCodes(v_data, v_frequencies);


    encodeHuffman(filename, "f_encoded.bernr", codes, wordSize);
    free(frequencies);

    decodeHuffman("f_encoded.bernr", "f_decoded.lukmak");


    return 0;
}
